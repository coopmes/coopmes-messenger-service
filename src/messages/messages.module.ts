import {Module} from '@nestjs/common';
import {MessagesService} from './messages.service';
import {MessagesController} from './messages.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {RoomChema} from "../rooms/chemas/room.chema";
import {UserChema} from "../users/chemas/user.chema";
import {MessageChema} from "./chemas/message.chema";
import {UsersService} from "../users/users.service";
import {RoomsService} from "../rooms/rooms.service";

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Room', schema: RoomChema}]),
        MongooseModule.forFeature([{name: 'User', schema: UserChema}]),
        MongooseModule.forFeature([{name: 'Message', schema: MessageChema}]),
    ],
    providers: [MessagesService, UsersService, RoomsService],
    controllers: [MessagesController]
})
export class MessagesModule {
}
