import * as mongoose from 'mongoose';

export const MessageChema = new mongoose.Schema({
    text: String,

    userId: {
        type: String,
        required: true
    },

    roomId: {
        type: String,
        required: true
    },

    createAt: {
        type: Date,
        required: true
    }
});
