import { Injectable } from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {UsersService} from "../users/users.service";
import {RoomsService} from "../rooms/rooms.service";
import {MessageInterface} from "./interfaces/message.interface";
import BaseException from "../exceptions/base.exception";
import {PushMessageDto} from "./dto/push-message.dto";
import {GetMessagesDto} from "./dto/get-messages.dto";

@Injectable()
export class MessagesService {
    constructor(
        @InjectModel('Message') private messageModel: Model<any>,
        private readonly usersService: UsersService,
        private readonly roomsService: RoomsService
    ) {}

    async pushMessage(pushMessageDto: PushMessageDto): Promise<MessageInterface[]> {
        await this.usersService.findById(pushMessageDto.userId);

        const room = await this.roomsService.findRoomById(pushMessageDto.roomId);

        if (!room.users.includes(pushMessageDto.userId)) {
            throw new BaseException('This user cannot push message in this room');
        }

        room.lastMessage = pushMessageDto.text;
        room.lastMessageDate = new Date();

        await room.save();

        const messageModel = new this.messageModel({
            ...pushMessageDto,
            createAt: new Date()
        });
        await messageModel.save();

        return messageModel;
    }

    async getMessages(getMessagesDto: GetMessagesDto): Promise<MessageInterface[]> {
        await this.usersService.findById(getMessagesDto.userId);

        const room = await this.roomsService.findRoomById(getMessagesDto.roomId);

        if (!room.users.includes(getMessagesDto.userId)) {
            throw new BaseException('This user cannot get messages in this room');
        }

        return this.getMessagesFromRoom(getMessagesDto.roomId, getMessagesDto.offset, getMessagesDto.limit);
    }

    private getMessagesFromRoom(roomId: string, offset: number, limit: number) {
        return this.messageModel.find({ roomId: roomId }, null, {
            skip: offset,
            limit: limit
        }).sort([['createAt', -1]]).exec()
    }
}
