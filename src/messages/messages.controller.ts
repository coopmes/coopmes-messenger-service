import { Controller } from '@nestjs/common';
import {MessagePattern} from "@nestjs/microservices";
import {MessagesService} from "./messages.service";
import {PushMessageDto} from "./dto/push-message.dto";
import {MessageInterface} from "./interfaces/message.interface";
import {GetMessagesDto} from "./dto/get-messages.dto";

@Controller('messages')
export class MessagesController {
    constructor(private readonly messagesService: MessagesService) {}

    @MessagePattern({ cmd: 'push_message' })
    pushMessage(pushMessageDto: PushMessageDto): Promise<MessageInterface[]> {
        return this.messagesService.pushMessage(pushMessageDto);
    }

    @MessagePattern({ cmd: 'get_messages' })
    getMessages(getMessageDto: GetMessagesDto): Promise<MessageInterface[]> {
        return this.messagesService.getMessages(getMessageDto);
    }
}
