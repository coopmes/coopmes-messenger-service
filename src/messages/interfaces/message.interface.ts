import { Document } from 'mongoose';

export interface MessageInterface extends Document {
    text: string,
    userId: string,
    roomId: string
}
