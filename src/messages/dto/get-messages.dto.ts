import {IsEmail, IsNotEmpty} from "class-validator";
import {PaginatorDto} from "../../dto/paginator.dto";

export class GetMessagesDto extends PaginatorDto {
    @IsNotEmpty()
    roomId: string;

    @IsNotEmpty()
    userId: string;
}
