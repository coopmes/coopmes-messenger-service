import {RpcException} from "@nestjs/microservices";

export default class BaseException extends RpcException {
    status: number;

    constructor(message: string, status: number = 500) {
        super(`(Messenger service) ${message}`);

        this.status = status;
    }

    getStatus() {
        return this.status;
    }
}