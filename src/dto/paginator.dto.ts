import {IsNotEmpty} from "class-validator";

export class PaginatorDto {
    @IsNotEmpty()
    offset: number;

    @IsNotEmpty()
    limit: number;
}