import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {
    Transport,
    MicroserviceOptions,
    ClientsModule,
} from '@nestjs/microservices';

async function bootstrap() {
    const app = await NestFactory.createMicroservice<MicroserviceOptions>(
        AppModule,
        {
            transport: Transport.NATS,
            options: {
                name: 'MESSENGER_SERVICE',
                url: 'nats://localhost:4222',
            },
        },
    );

    app.listen(() => console.log('Messenger microservice is listening'));
}

bootstrap();
