import * as mongoose from 'mongoose';

export const UserChema = new mongoose.Schema({
    name: {
        type: String,
        default: ''
    },

    email: {
        type: String,
        trim: true,
        unique: true
    },

    password: String,

    rooms: {
        /**
         * id rooms
         */
        type: Array,
        default: []
    },

    friends: {
        /**
         * id confirm friends
         */
        type: Array,
        default: []
    },

    notAllowedFriends: {
        /**
         * id not confirm friends
         */
        type: Array,
        default: []
    },

    bannedUsers: {
        /**
         * id banned friends
         */
        type: Array,
        default: []
    },

    reqToAllowedFriends: {
        /**
         * id need confirm friend
         */
        type: Array,
        default: []
    },

    confirmAuth: {
        type: Boolean,
        default: false
    },

    confirmToken: {
        type: String,
        default: ''
    }
});
