import { Controller, Get } from '@nestjs/common';
import { UsersService } from './users.service';
import {MessagePattern} from "@nestjs/microservices";
import {User} from "./interfaces/user.interface";
import {CreateUserDto} from "./dto/create-user.dto";
import {LoginEmailPasswordDto} from "./dto/login-email-password.dto";
import {ConfirmAuthUserDto} from "./dto/confirm-auth-user.dto";

@Controller()
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

    @Get()
    getHello(): string {
        return '';
    }

    @MessagePattern({ cmd: 'add_user' })
    async createUser(userDto: CreateUserDto): Promise<User> {
        return this.usersService.create(userDto);
    }

    @MessagePattern({ cmd: 'get_users' })
    async getUsers(fields): Promise<User[]> {
        const users = await this.usersService.getUsers(fields);
        return users;
    }

    @MessagePattern({cmd: 'email_login'})
    async emailLogin(loginPassword: LoginEmailPasswordDto): Promise<User> {
        const userId = await this.usersService.getUserIdValidatePassword(loginPassword);
        return this.usersService.findById(userId);
    }

    @MessagePattern({ cmd: 'find_user_by_id' })
    async findUserById(userId: string): Promise<User> {
        return this.usersService.findById(userId);
    }

    @MessagePattern({ cmd: 'auth' })
    async auth(fields: LoginEmailPasswordDto): Promise<User> {
        return this.usersService.auth(fields);
    }

    @MessagePattern({ cmd: 'confirm_auth_user' })
    confirmAuthUser(fields: ConfirmAuthUserDto): Promise<User> {
        return this.usersService.confirmUserAuth(fields.token);
    }

    @MessagePattern({ cmd: 'delete_email_user' })
    deleteEmailUser(email: string) {
        return this.usersService.deleteEmailUser(email);
    }

    @MessagePattern({ cmd: 'add_friend' })
    reqAddFriend(fields) {
        return this.usersService.addFriend(fields.id, fields.friendEmail);
    }

    @MessagePattern({ cmd: 'remove_friend' })
    removeFriend(fields) {
        return this.usersService.removeFrined(fields.id, fields.friendId);
    }

    @MessagePattern({ cmd: 'allowed_friend' })
    allowedFriend(fields) {
        return this.usersService.allowedFriend(fields.id, fields.friendId);
    }

    @MessagePattern({ cmd: 'not_allowed_friend' })
    notAllowedFriend(fields) {
        return this.usersService.notAllowedFriend(fields.id, fields.friendId);
    }

    @MessagePattern({ cmd: 'get_users_from_ids' })
    getUsersFromIds(fields) {
        return this.usersService.getUsersFromIds(fields.users)
    }
}
