import {HttpException, Injectable, BadRequestException} from '@nestjs/common';
import {User} from './interfaces/user.interface';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import {CreateUserDto} from "./dto/create-user.dto";
import {LoginEmailPasswordDto} from "./dto/login-email-password.dto";
import * as bcrypt from 'bcrypt';
import * as crypto from 'crypto';
import {RpcException} from "@nestjs/microservices";
import BaseException from "../exceptions/base.exception";

@Injectable()
export class UsersService {
    constructor(@InjectModel('User') private userModel: Model<any>) {}

    private async findByEmail(email: string): Promise<User> {
        return await this.userModel.findOne({email: email}).exec()
    }

    private findByName(name: string): Promise<User> {
        return this.userModel.findOne({name: name}).exec();
    }

    private findByConfirmToken(token: string): Promise<User> {
        return this.userModel.findOne({confirmToken: token}).exec()
    }

    async findById(id: string): Promise<User> {
        try {
            return await this.userModel.findById(id).exec();
        } catch (e) {
            return null
        }
    }

    async getUserIdValidatePassword(emailPassowrd: LoginEmailPasswordDto): Promise<string> {
        const user = await this.findByEmail(emailPassowrd.email);

        if (!user) {
            return '';
        }

        return user.password === emailPassowrd.password ? user._id : '';
    }

    async create(createUserDto: CreateUserDto): Promise<User> {
        let user = await this.findByEmail(createUserDto.email);

        if (user) {
            if (user.confirmAuth) {
                throw new BaseException('This email exist in system');
            }
        } else {
            user = new this.userModel(createUserDto);
        }

        user.confirmToken = crypto.randomBytes(20).toString('hex');;

        await user.save();

        return user;
    }

    async getUsers(fields): Promise<User[]> {
        if (fields.query) {
            return this.userModel.find({
                $or: [{
                    name: { $regex: '.*' + fields.query + '.*' },
                }, {
                    email: { $regex: '.*' + fields.query + '.*' },
                }]
            })
        }

        if (fields.users) {
            return this.userModel.find({
                $in: {
                    _id: fields.users
                }
            })
        }

        return this.userModel.find();
    }

    async auth(emailPassword: LoginEmailPasswordDto): Promise<User> {
        /**
         * todo Имя не уникальное поле
         */

        let user = await this.findByName(emailPassword.email);

        if (!user) {
           user = await this.findByEmail(emailPassword.email);
        }

        return user;
    }

    async confirmUserAuth(token: string): Promise<User> {
        const user = await this.findByConfirmToken(token);

        if (!user) {
            return null;
        }

        if (user.confirmAuth) {
            return null;
        }

        user.confirmAuth = true;
        user.confirmToken = '';

        try {
            await user.save();
            return user;
        } catch (e) {
            console.log(e);
            return null;
        }
    }

    async deleteEmailUser(email: string) {
        const user = await this.findByEmail(email);

        if (user) {
            return user.delete();
        }

        return false;
    }

    async getUserById(id): Promise<User> {
        const user = await this.findById(id);

        if (!user) {
            throw new BaseException('User not found');
        }

        return user;
    }

    /**
     * Отправить запрос на добавления в друзья
     * @param id
     * @param email
     */
    async addFriend(id: string, email: string) {
        const user = await this.getUserById(id);
        const friend = await this.findByEmail(email);

        if (!friend) {
            throw new BaseException('Friend not found')
        }

        try {
            await this.pushNotAllowedUsers(user._id, friend).save();
            await this.pushReqToAllowedUser(friend._id, user).save();
        } catch (e) {
            throw new BaseException(e.message);
        }

        return user;
    }

    private pushNotAllowedUsers(id: string, user: User): User {
        if (user.friends.includes(id)) {
            return user;
        }

        if (user.notAllowedFriends.includes(id)) {
            return user;
        }

        user.notAllowedFriends.push(id);

        return user;
    }

    private pushReqToAllowedUser(id: string, user: User): User {
        if (user.friends.includes(id)) {
            return user;
        }

        if (user.reqToAllowedFriends.includes(id)) {
            return user;
        }

        user.reqToAllowedFriends.push(id);

        return user;
    }

    private filterNotAllowedFriends(id: string, user: User): User {
        if (user.friends.includes(id)) {
            return user;
        }

        if (!user.notAllowedFriends.includes(id)) {
            return user;
        }

        user.notAllowedFriends = user.notAllowedFriends.filter(_id => _id !== id);

        return user;
    }

    private filterReqToAllowedUser(id: string, user: User): User {
        if (user.friends.includes(id)) {
            return user;
        }

        if (!user.reqToAllowedFriends.includes(id)) {
            return user;
        }

        user.reqToAllowedFriends = user.reqToAllowedFriends.filter(_id => _id !== id);

        return user;
    }

    private getUsersIds(ids): Promise<User[]> {
        return this.userModel.find().where('_id').in(ids).exec();
    }

    /**
     * Получить добавленных друзей
     * @param id
     */
    async getAllowedFriends(id: string) {
         const user =  await this.getUserById(id);
         return this.getUsersIds(user.friends);
    }

    /**
     * Получить друзей, которые еще не потвердили
     * @param id
     */
    async getNotAllowedFriends(id: string) {
        const user = await this.getUserById(id);
        return this.getUsersIds(user.notAllowedFriends);
    }

    /**
     * Список забаненых юзеров
     * @param id
     */
    async getBannedFriends(id: string) {
        const user = await this.getUserById(id);
        return this.getUsersIds(user.bannedUsers);
    }

    /**
     * Список кто хочет добавиться в друзья
     * @param id
     */
    async getReqToAllowedFriends(id: string) {
        const user = await this.getUserById(id);
        return this.getUsersIds(user.reqToAllowedFriends);
    }

    private pushFriend(user: User, friendId: string): User {
        if (user.friends.includes(friendId)) {
            return user;
        }

        user.friends.push(friendId);
        return user;
    }

    /**
     * Удалить друга
     * @param user
     * @param friendId
     */
    private _removeFrined(user: User, friendId: string): User {
        if (user.friends.includes(friendId)) {
            return user;
        }

        user.friends = user.friends.filter(_id => _id !== friendId);

        return user;
    }

    /**
     * Потвердить друга
     * @param userId
     * @param friendId
     */
    async allowedFriend(userId: string, friendId: string) {
        const user = await this.getUserById(userId);
        const friend = await this.getUserById(friendId);

        try {
            await this.filterNotAllowedFriends(
                user._id,
                this.pushFriend(friend, user._id)
                ).save();

            await this.filterReqToAllowedUser(
                friend._id,
                this.pushFriend(user, friendId)
            ).save();
        } catch (e) {
            throw new BaseException(e.message);
        }

        return user;
    }

    /**
     * Отменить потверждение в друзья
     * @param userId
     * @param friendId
     */
    async notAllowedFriend(userId: string, friendId: string) {
        const user = await this.getUserById(userId);
        const friend = await this.getUserById(friendId);

        try {
            await this.filterNotAllowedFriends(
                user._id,
                friend
            ).save();

            await this.filterReqToAllowedUser(
                friend._id,
                user
            ).save();
        } catch (e) {
            throw new BaseException(e.message);
        }

        return user;
    }

    async banUser(userId: string, friendId: string) {
        const user = await this.getUserById(userId);
        const friend = await this.getUserById(friendId);
    }

    async removeFrined(userId: string, friendId: string) {
        const user = await this.getUserById(userId);
        const friend = await this.getUserById(friendId);

        try {
            await this._removeFrined(user, friend._id).save();
            await this._removeFrined(friend, user._id).save();
        } catch (e) {
            throw new BaseException(e.message);
        }

        return user;
    }

    async appendToRoom(userId: string, roomId: string): Promise<User> {
        const user = await this.getUserById(userId);

        if (!user.rooms.includes(roomId)) {
            user.rooms.push(roomId);
            await user.save()
        }

        return user;
    }

    async leaveFromRoom(userId: string, roomId: string): Promise<User> {
        const user = await this.getUserById(userId);

        if (user.rooms.includes(roomId)) {
            user.rooms = user.rooms.filter(id => id !== roomId);
            await user.save();
        }

        return user;
    }

    async getUsersFromIds(userIds: []): Promise<User[]> {
        return this.userModel.find({
            id: {
                $in: userIds
            }
        })
    }
}
