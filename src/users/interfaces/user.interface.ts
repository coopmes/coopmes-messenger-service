import { Document } from 'mongoose';

export interface User extends Document {
    name: string,

    email: string,
    password: string,

    rooms: any,

    friends: any,
    notAllowedFriends: any,
    bannedUsers: any,
    reqToAllowedFriends: any,

    confirmAuth: boolean,
    confirmToken: string
}
