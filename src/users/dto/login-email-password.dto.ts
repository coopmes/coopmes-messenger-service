import {IsEmail, IsNotEmpty} from "class-validator";

export class LoginEmailPasswordDto {
    @IsEmail()
    email: string;

    @IsNotEmpty()
    password: string;
}
