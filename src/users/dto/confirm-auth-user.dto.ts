import {IsEmail, IsNotEmpty} from "class-validator";

export class ConfirmAuthUserDto {
    @IsNotEmpty()
    token: string;
}
