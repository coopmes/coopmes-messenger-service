import * as mongoose from 'mongoose';

export const RoomChema = new mongoose.Schema({
    name: String,

    users: {
        type: Array,
        default: []
    },

    administrators: {
        type: Array,
        default: []
    },

    lastMessage: {
        type: String,
        default: ''
    },

    lastMessageDate: {
        type: Date,
        default: ''
    }
});
