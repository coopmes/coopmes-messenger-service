import {IsEmail, IsNotEmpty} from "class-validator";
import {PaginatorDto} from "../../dto/paginator.dto";

export class GetRoomsDto extends PaginatorDto {
    @IsNotEmpty()
    userId: string;
}
