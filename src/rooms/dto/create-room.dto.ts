import {IsEmail, IsNotEmpty} from "class-validator";

export class CreateRoomDto {
    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    users: string[];

    @IsNotEmpty()
    userId: string;
}
