import {IsEmail, IsNotEmpty} from "class-validator";

export class PutRoomDto {
    @IsNotEmpty()
    id: string;

    @IsNotEmpty()
    users: string[];

    @IsNotEmpty()
    administrators: string[];

    @IsNotEmpty()
    userId: string;

    @IsNotEmpty()
    name: string;
}
