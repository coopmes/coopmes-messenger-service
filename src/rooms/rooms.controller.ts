import { Controller } from '@nestjs/common';
import {MessagePattern} from "@nestjs/microservices";
import {CreateRoomDto} from "./dto/create-room.dto";
import {RoomInterface} from "./interfaces/room.interface";
import {UsersService} from "../users/users.service";
import {RoomsService} from "./rooms.service";
import {PutRoomDto} from "./dto/put-room.dto";
import {GetRoomsDto} from "./dto/get-rooms.dto";

@Controller('rooms')
export class RoomsController {
    constructor(private readonly roomsService: RoomsService) {}

    @MessagePattern({ cmd: 'create_room' })
    createRoom(roomDto: CreateRoomDto): Promise<RoomInterface> {
        return this.roomsService.createRoom(roomDto);
    }

    @MessagePattern({ cmd: 'put_room' })
    putRoom(putRoomDto: PutRoomDto): Promise<RoomInterface> {
        return this.roomsService.putRoom(putRoomDto);
    }

    @MessagePattern({ cmd: 'get_rooms' })
    getRooms(getRoomsDto: GetRoomsDto): Promise<RoomInterface[]> {
        return this.roomsService.getRoomsForUser(getRoomsDto);
    }
}
