import { Injectable } from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {CreateRoomDto} from "./dto/create-room.dto";
import {RoomInterface} from "./interfaces/room.interface";
import {PutRoomDto} from "./dto/put-room.dto";
import BaseException from "../exceptions/base.exception";
import {UsersService} from "../users/users.service";
import {GetRoomsDto} from "./dto/get-rooms.dto";

@Injectable()
export class RoomsService {
    constructor(
        @InjectModel('Room') private roomModel: Model<any>,
        private readonly usersService: UsersService
    ) {}

    async createRoom(roomDto: CreateRoomDto): Promise<RoomInterface> {
        if (roomDto.users.length === 1) {
            const rooms = await this.getRoomUserToUser(roomDto.users[0], roomDto.userId);

            if (rooms.length > 0) {
                return rooms[0];
            }
        }

        const room = new this.roomModel(roomDto);

        room.users.push(roomDto.userId);
        room.administrators.push(roomDto.userId);

        return room.save();
    }

    async putRoom(putRoomDto: PutRoomDto): Promise<RoomInterface> {
        const room = await this.roomModel.findById(putRoomDto.id);

        if (!room) {
            throw new BaseException('Not found room');
        }

        if (!room.administrators.includes(putRoomDto.userId)) {
            throw new BaseException('Not access user put room');
        }

        if (!putRoomDto.administrators.includes(putRoomDto.userId)) {
            throw new BaseException('User cannot self leave from administrator group');
        }

        putRoomDto.administrators.forEach(adminId => {
            if (!putRoomDto.users.includes(adminId)) {
                throw new BaseException('Fail save. Invalid signature room. Administrator id not linked in users.');
            }
        })

        room.administrators = putRoomDto.administrators;

        for (const userId of room.users) {
            if (!putRoomDto.users.includes(userId)) {
                await this.usersService.leaveFromRoom(userId, putRoomDto.id)
            }
        }

        room.users = putRoomDto.users;

        for (const userId of room.users) {
            await this.usersService.appendToRoom(userId, putRoomDto.id);
        }

        room.name = putRoomDto.name;

        await room.save();

        return room;
    }

    async findRoomById(roomId: string): Promise<RoomInterface> {
        const room = await this.roomModel.findById(roomId);

        if (!room) {
            throw new BaseException(`Not found roomId: ${roomId}`)
        }

        return room;
    }

    /**
     * Взять приватную беседу двух юзеров
     * @param userId
     * @param friendId
     */
    getRoomUserToUser(userId: string, friendId: string): Promise<RoomInterface[]> {
        return this.roomModel.find({users: { $all: [userId, friendId] }}).exec();
    }

    async getRoomsForUser(getRoomsDto: GetRoomsDto): Promise<RoomInterface[]> {
        const rooms = await this.roomModel.find({ users : { $all : [getRoomsDto.userId] } }, null, {
            skip: getRoomsDto.offset, limit: getRoomsDto.limit
        }).exec();

        let _rooms = [];

        for (const room of rooms) {
            if (room.users.length === 2) {
                const userId = room.users.filter(id => id !== getRoomsDto.userId)[0];
                if (userId) {
                    const user = await this.usersService.findById(userId);

                    room.name = user.email;
                }
            }

            _rooms.push(room);
        }

        return _rooms;
    }
}
