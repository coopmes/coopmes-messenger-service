import { Document } from 'mongoose';

export interface RoomInterface extends Document {
    name: string,
    users: string[],
    administrators: string[],
    lastMessage: string,
    lastMessageDate: Date
}
